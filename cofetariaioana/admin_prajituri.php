<?php
require_once 'util/GenericController.php';
require_once 'services/AdminPrajituriService.php';

class AdminPrajituriController extends GenericController{
	
	public static function getDefault(){
		$model = AdminPrajituriService::getPrajituri();
		
		include 'view/admin_prajituri_view.php';
	}
	public static function getEdit(){
		$model = AdminPrajituriService::getPrajituraDupaId();
		
		include 'view/admin_prajituri_edit_view.php';
	}
	
	public static function getUpdate(){
		$model = AdminPrajituriService::updatePrajitura();
		header('Location: admin_prajituri.php');
	}
	
	public static function getAdd(){
		$model = AdminPrajituriService::newPrajituraModel();
		include 'view/admin_prajituri_edit_view.php';
	}
	public static function getDelete(){
		AdminPrajituriService::stergePrajitura();
		header('Location: admin_prajituri.php');
	}
}


AdminPrajituriController::resolveAction();
?>