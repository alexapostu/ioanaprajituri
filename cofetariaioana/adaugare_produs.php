<?php
require_once 'util/GenericController.php';
require_once 'services/AdaugareProdusService.php';

class AdaugareProdusController extends GenericController {
	public static function getDefault() {
		AdaugareProdusService::adaugaProdusLaComanda ();
		
		header ( 'Location: shopping.php' );
	}
	
	public static function getStergeProdus() {
		AdaugareProdusService::stergeProdusDinComanda ();
		
		header ( 'Location: ' . $_SESSION['url'] );
	}
	public static function getAdaugaAdresa(){
		AdaugareProdusService::adaugaAdresa();
		header ( 'Location: ' . $_SESSION['url'] );
	}
}

AdaugareProdusController::resolveAction ();
?>