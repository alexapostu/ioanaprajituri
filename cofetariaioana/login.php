<?php
require_once 'util/GenericController.php';
require_once 'services/LoginService.php';
class LoginController extends GenericController {
	public static function getDefault() {
		include 'view/login_view.php';
	}
	public static function getLogin() {
		$model = LoginService::tryLogin ();
		if ($model->loginSuccessful) {
			if ($_SESSION ['user']->type == 3) {
				header ( 'Location: comenzi.php' );
			}
			header ( 'Location: home.php' );
		} else {
			include 'view/login_view.php';
		}
	}
}

LoginController::resolveAction ();
?>