<?php
/**
 * 
 * @author Ioana Simona Despu
 * Controller de baza ce trebuie extins de toate celelalte controllere
 * Ofera functionalitatile de baza pentru routarea actiunilor
 */
class GenericController{
	public static function resolveAction(){
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
		
		if( isset( $_POST['action']) ){
			static::{getActionFunction( $_POST['action'] )}();
		}else if( isset( $_GET['action']) ){
			static::{getActionFunction( $_GET['action'] )}();
		}else{
			static::{getActionFunction('default')}();
		}
	}
}
/**
 * Return a camelCase function name based on actionName.
 * E.g. : for $actionName = "default", return value is "getDefault"
 * @param string $actionName
 * @return string
 */
function getActionFunction($actionName){
	return 'get' . ucwords($actionName);
}

?>