<?php
require_once 'util/GenericController.php';
require_once 'services/ShoppingService.php';

class ShoppingController extends GenericController{

	public static function getDefault(){
		$model = ShoppingService::getShopping();
				
		include 'view/shopping_view.php';
	}	
}

ShoppingController::resolveAction();
?>