CREATE DATABASE `cofetariaioana`;
use 'cofetariaioana';
CREATE TABLE `users` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `login` varchar(32) NOT NULL,
 `password` varchar(256) NOT NULL,
 `type` int(11) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO users(login, password, type) values('admin', 'admin', '1');