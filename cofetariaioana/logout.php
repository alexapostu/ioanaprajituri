<?php
require_once 'util/GenericController.php';
require_once 'services/LogoutService.php';

class LogoutController extends GenericController{
	
	public static function getDefault(){
		LogoutService::doLogout();
		header('Location: home.php');
	}
}

LogoutController::resolveAction();
?>