<?php
require_once 'util/GenericController.php';
require_once 'services/ComenziService.php';
class ComenziController extends GenericController {
	public static function getDefault() {
		$model = ComenziService::getComenzi ();
		include 'view/comenzi_view.php';
	}
	public static function getMarcheazaLivrat() {
		ComenziService::marcheazaLivrat();
		header("Location:comenzi.php");
	}
}

ComenziController::resolveAction ();
?>