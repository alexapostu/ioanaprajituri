<?php
require './dao/AdaugareProdusDAO.php';
require './dao/AdminPrajituriDAO.php';
require './dao/MenuDAO.php';
class AdaugareProdusService {
	public static function adaugaProdusLaComanda() {
		$id = $_POST ['idPrajitura'];
		$produs = AdminPrajituriDAO::getPrajituraDupaId ( $id );
		$produs->cantitateProdusDeAdaugat = $_POST ['cantitateProdusDeAdaugat'];
		$produs->pret = MenuDAO::getPretDupaIDPrajitura ( $produs->id );
		self::adaugaProdusInComanda ( $produs );
	}
	public static function stergeProdusDinComanda() {
		$id = $_GET ['idPrajitura'];
		if (isset ( $_SESSION ['comanda'] ) && isset ( $_SESSION ['comanda']->prajituri )) {
			unset ( $_SESSION ['comanda']->prajituri [$id] );
		}
	}
	public static function adaugaAdresa() {
		$adresa = null;
		if (isset ( $_POST ['adresa'] )) {
			$adresa = $_POST['adresa'];
		}
		
		if($adresa != null){
			AdaugareProdusDAO::adaugaAdresa( $adresa, $_SESSION['user']->id);
		}
	}
	private static function adaugaProdusInComanda($produs) {
		if (! isset ( $_SESSION ['comanda'] )) {
			$_SESSION ['comanda'] = ( object ) [ 
					'prajituri' => [ ] 
			];
		}
		$_SESSION ['comanda']->prajituri [$produs->id] = $produs;
	}
}

?>