<?php
require_once './dao/AdminPrajituriDAO.php';
require_once './dao/MenuDAO.php';
class AdminPrajituriService {
	function getPrajituri() {
		$model = ( object ) [ ];
		
		$prajituri = MenuDAO::getPrajituri ();
		$model->prajituri = $prajituri;
		
		return $model;
	}
	public static function getPrajituraDupaId() {
		$id = $_GET ['id'];
		$model = AdminPrajituriDAO::getPrajituraDupaId ( $id );
		$model->categorii = MenuDAO::getCategoriiPrajituri ();
		
		return $model;
	}
	public static function updatePrajitura() {
		$prajitura = self::getPrajituraParameters ();
		if ($prajitura->id != '') {
			AdminPrajituriDAO::updatePrajitura ( $prajitura );
		} else {
			AdminPrajituriDAO::addPrajitura ( $prajitura );
		}
	}
	public static function newPrajituraModel() {
		$model = ( object ) [ 
				"id" => "",
				"descriere" => "",
				"nume" => "",
				"poza" => "",
				"categorie" => - 1 
		];
		$model->categorii = MenuDAO::getCategoriiPrajituri ();
		return $model;
	}
	public static function stergePrajitura() {
		$id = $_GET ['id'];
		AdminPrajituriDAO::stergePrajituraDupaID ( $id );
	}
	private static function getPrajituraParameters() {
		$obj = ( object ) [ 
				'id' => $_POST ['id'],
				'nume' => $_POST ['nume'],
				'descriere' => $_POST ['descriere'],
				'poza' => $_POST ['poza'],
				'pret' => $_POST ['pret'],
				'categorie' => $_POST ['categorie'] 
		];
		if ($obj->categorie == - 1) {
			$obj->categorie = null;
		}
		
		return $obj;
	}
}

?>