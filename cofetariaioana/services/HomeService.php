<?php
require_once './dao/MenuDAO.php';
class HomeService {
	public static function getMenus() {
		$model = ( object ) [ ];
		
		$categorii_prajituri = MenuDAO::getCategoriiPrajituri ();
		$prajituri = MenuDAO::getPrajituri ();
		
		$model->categorii_prajituri = $categorii_prajituri;
		$model->prajituri = $prajituri;
		
		return $model;
	}
}

?>