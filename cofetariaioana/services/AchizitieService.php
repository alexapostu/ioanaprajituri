<?php
require './dao/AchizitieDAO.php';
class AchizitieService {
	public static function getComenzi() {
		$model = $_SESSION ['comanda'];
	}
	public static function getAdrese() {
		$idUtilizator = null;
		if (isset ( $_SESSION ['user'] )) {
			$idUtilizator = $_SESSION ['user']->id;
		}
		if ($idUtilizator == null) {
			return null;
		}
		$model = ( object ) [ ];
		$model->adrese = AchizitieDAO::getAdreseDupaUtilizator ( $idUtilizator );
		return $model;
	}
	public static function finalizareComanda() {
		$model = ( object ) [ 
				'errors' => [ ]
		];
		$adresa = self::obtineAdresaDinPagina ();
		if ($adresa == null) {
			array_push ( $model->errors, "Adresa de livrare nu poate fi goala!" );
		}
		if (! isset ( $_SESSION ['comanda'] ) || ! isset ( $_SESSION ['comanda']->prajituri ) || count ( $_SESSION ['comanda']->prajituri ) == 0) {
			array_push ( $model->errors, "Lista de produse nu poate fi goala!" );
		}
		
		if (count ( $model->errors ) == 0) {
			AchizitieDAO::inregistrareComanda ( $adresa, $_SESSION ['comanda']->prajituri );
			$model->success = 'Comanda a fost plasata cu success!';
			unset( $_SESSION['comanda'] );
		}
		
		return $model;
	}
	private static function obtineAdresaDinPagina() {
		$adresa = null;
		if (isset ( $_POST ['adresa'] )) {
			$adresa = $_POST ['adresa'];
		}
		return $adresa;
	}
}

?>