<?php
require_once './dao/AdminPromotiiDAO.php';
require_once './dao/PromotieDAO.php';
require_once './dao/MenuDAO.php';
class AdminPromotiiService {
	public static function getPromotiiSiPrajituri() {
		$model = ( object ) [ ];
		
		$promotii = PromotieDAO::getPromotiiSiPrajituri ();
		$model->promotii = $promotii;
		
		return $model;
	}
	public static function getPromotieDupaId() {
		$id = $_GET ['id'];
		$model = AdminPromotiiDAO::getPromotieDupaId ( $id );
		$model->prajituri = MenuDAO::getPrajituri ();
		
		return $model;
	}
	public static function updatePromotie() {
		$promotie = self::getPromotieParameters ();
		if ($promotie->id != '') {
			AdminPromotiiDAO::updatePromotie ( $promotie );
		} else {
			AdminPromotiiDAO::addPromotie ( $promotie );
		}
	}
	public static function newPromotieModel() {
		$model = ( object ) [ 
				"id" => "",
				"descriere" => "",
				"nume" => "",
				"poza" => "" 
		];
		$model->prajituri = MenuDAO::getPrajituri ();
		
		return $model;
	}
	public static function stergePromotia() {
		$id = $_GET ['id'];
		AdminPromotiiDAO::stergePromotiaDupaID ( $id );
	}
	private static function getPromotieParameters() {
		$obj = ( object ) [ 
				'id' => $_POST ['id'],
				'id_prajitura' => $_POST ['id_prajitura'],
				'pret' => $_POST ['pret'] 
		];
		return $obj;
	}
}

?>