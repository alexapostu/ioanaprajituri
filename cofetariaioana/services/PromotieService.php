<?php
require_once './dao/PromotieDAO.php';

class PromotieService{
	public static function getPromotiisiPrajituri(){
		$model = (object) [];
		
		$promotiiSiPrajituri = PromotieDAO::getPromotiiSiPrajituri();
		
		$model->promotiiSiPrajituri = $promotiiSiPrajituri;
		
		return $model;
	}
}

?>