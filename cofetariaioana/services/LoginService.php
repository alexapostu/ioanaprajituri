<?php
require_once './dao/LoginDAO.php';

class LoginService{
	public static function tryLogin(){
		$model = (object) ['loginSuccessful' => false];
		
		
		$loginParameters = self::getLoginParameters();
		$user = LoginDAO::getUserByLoginAndPass($loginParameters);
		if( $user != null ){
			self::registerUserOnSession($user);
			$model->loginSuccessful = true;
		}else{
			$model->error = "Userul sau parola nu sunt corecte!";
		}
		
		return $model;
	}
	
	private static function getLoginParameters(){
		$obj = (object) [
			'login' => $_POST['logare'],
			'password' => $_POST['parola']
		];
		
		return $obj;
	}
	
	private static function registerUserOnSession($user){
		session_start();
		$_SESSION['user'] = $user;
	}
}

?>