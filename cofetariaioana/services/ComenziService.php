<?php
require './dao/ComenziDAO.php';
class ComenziService {
	public static function getComenzi() {
		$model = ( object ) [ ];
		
		$model->comenzi = ComenziDAO::getToateComenzile ();
		$model->produse = ComenziDAO::getProduseComandate ();
		return $model;
	}
	
	public static function marcheazaLivrat(){
		$idComanda = $_GET['id'];
		ComenziDAO::marcheazaLivrat($idComanda);
	}
}

?>