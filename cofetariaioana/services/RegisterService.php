<?php
require_once './dao/RegisterDAO.php';
class RegisterService {
	public function inregistrare() {
		$model = (object)[
				'inregistrareReusita' => false,
				'errors' => []
		];
		$detalii = self::getDetalii ();
		if (RegisterDAO::existaUtilizator ( $detalii->user )) {
			array_push($model->errors, "Utilizatorul cu numele <b>" . $detalii->user . '</b> exista deja.');
		}
		if ( $detalii->pass != $detalii->repass ) {
			array_push($model->errors, "Cele doua parole nu sunt identice.");
		}
		if( count( $model->errors) == 0 ){
			$model->inregistrareReusita = true;
			$model->success = "Utilizatorul <b>" . $detalii->user . "</b> a fost creat cu succes.";
			RegisterDAO :: inregistrareUtilizator( $detalii->user, $detalii->pass );
		}
		
		return $model;
	}
	private function getDetalii() {
		$obj = ( object ) [ 
				'user' => $_POST ['utilizator'],
				'pass' => $_POST ['parola'],
				'repass' => $_POST ['rep_parola'] 
		];
		return $obj;
	}
}

?>