<?php
require_once 'util/GenericController.php';

class AboutUsController extends GenericController {
	public static function getDefault() {
		
		include 'view/aboutus_view.php';
	}
}

AboutUsController::resolveAction ();
?>