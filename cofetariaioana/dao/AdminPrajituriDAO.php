<?php

class AdminPrajituriDAO{
	public static function getPrajituraDupaId( $id){
		require("./util/DBConnector.php");
		
		$prajitura = null;
		
		$stmt = $conn->prepare("SELECT id, nume, descriere, pret, categorie, poza FROM prajituri WHERE id = ?");
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt->bind_result($id,$nume,$descriere,$pret,$categorie, $poza);
		$stmt->fetch();
		$stmt->close();
		
		if($id != null){
			$prajitura= (object) [
					'id' => $id,
					'nume' => $nume,
					'descriere'=>$descriere,
					'pret'=>$pret,
					'categorie'=>$categorie,
					'poza'=>$poza
			];
		}
		
		return $prajitura;
	}
	
	public static function updatePrajitura($prajitura){
		require("./util/DBConnector.php");
		$updateQuery = "UPDATE prajituri SET descriere = ?, pret = ?, categorie = ?, nume = ?, poza = ? WHERE id = ?";
		$stmt = $conn->prepare( $updateQuery);
		
		$stmt->bind_param("sdissi", 
				$prajitura->descriere,
				$prajitura->pret,
				$prajitura->categorie,
				$prajitura->nume,
				$prajitura->poza,
				$prajitura->id);
		$stmt->execute();
		$stmt->close();
	}
	public static function addPrajitura($prajitura){
		require("./util/DBConnector.php");
		$insertQuery = "INSERT INTO prajituri(descriere, pret, categorie, nume, poza) VALUES(?, ?, ?, ?, ?)";
		$stmt = $conn->prepare( $insertQuery);
		
		$stmt->bind_param("sdiss",
				$prajitura->descriere,
				$prajitura->pret,
				$prajitura->categorie,
				$prajitura->nume,
				$prajitura->poza);
		$stmt->execute();
		$stmt->close();
	}
	public static function stergePrajituraDupaID($id){
		require("./util/DBConnector.php");
		$insertQuery = "DELETE FROM prajituri WHERE id = ?";
		$stmt = $conn->prepare( $insertQuery);
		
		$stmt->bind_param("i",$id);
		$stmt->execute();
		$stmt->close();
	}
}

?>