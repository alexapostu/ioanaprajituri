<?php

class PromotieDAO{
	
	public static function getPromotii(){
		require("./util/DBConnector.php");
		$promotii = [];
		
		$stmt = $conn->prepare("SELECT id, pret, id_prajitura FROM promotie");
		$stmt->execute();
		$stmt->bind_result($id, $pret, $id_prajitura);
		while($stmt->fetch()){
			$obj = (object) [
					'id' => $id,
					'pret' => $pret,
					'id_prajitura'=>$id_prajitura
			];
			array_push($promotii,$obj);
		}
		
		
		$stmt->close();
		
		return $promotii;
	}
	
	public static function getPromotiiSiPrajituri(){
		require("./util/DBConnector.php");
		$prajituri = [];
	

		$stmt = $conn->prepare("SELECT promo.id, praji.nume, praji.descriere, promo.pret, praji.pret, praji.poza, praji.id
		FROM promotie promo INNER JOIN prajituri praji ON promo.id_prajitura = praji.id");
		
		$stmt->execute();
		$stmt->bind_result($id,$nume,$descriere,$pretNou, $pretVechi ,$poza, $id_prajitura);
		while($stmt->fetch()){
			$obj = (object) [
				'id' => $id,
				'nume' => $nume,
				'descriere'=>$descriere,
				'pretNou'=>$pretNou,
				'pretVechi'=>$pretVechi,
				'poza'=>$poza,
				'id_prajitura' => $id_prajitura
			];
			array_push($prajituri,$obj);
		}
	
		$stmt->close();
	
		return $prajituri;
	}
}
?>