<?php

class LoginDAO{
	public static function getUserByLoginAndPass( $loginParameters){
		require("./util/DBConnector.php");
		
		$user = null;
		
		$stmt = $conn->prepare("SELECT id, login, type FROM users WHERE login = ? AND password = ?");
		$stmt->bind_param("ss", $loginParameters->login, $loginParameters->password);
		$stmt->execute();
		$stmt->bind_result($id, $login, $type);
		$stmt->fetch();
		$stmt->close();
		
		if($id != null){
			$user = (object)[
				'login' => $login,
				'type' => $type,
				'id' => $id
			];
		}
		
		echo $type;
		
		return $user;
	}
}

?>