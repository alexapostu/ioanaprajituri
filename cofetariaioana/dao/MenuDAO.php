<?php

class MenuDAO{
	
	public static function getCategoriiPrajituri(){
		require("./util/DBConnector.php");
		$categorii = [];
		
		$stmt = $conn->prepare("SELECT id,descriere,poza FROM categorie_prajituri");
		$stmt->execute();
		$stmt->bind_result($id,$descriere,$poza);
		while($stmt->fetch()){
			$obj = (object) [
					'id' => $id,
					'descriere' => $descriere,
					'poza'=>$poza
			];
			array_push($categorii,$obj);
		}
		
		$stmt->close();
		
		return $categorii;
	}
	
	public static function getPrajituri(){
		require("./util/DBConnector.php");
		$prajituri = [];
	
		$stmt = $conn->prepare("SELECT id, nume, descriere, pret, categorie, poza FROM prajituri ORDER BY id DESC");
		$stmt->execute();
		$stmt->bind_result($id,$nume,$descriere,$pret,$categorie,$poza);
		while($stmt->fetch()){
			$obj = (object) [
					'id' => $id,
					'nume' => $nume,
					'descriere'=>$descriere,
					'pret'=>$pret,
					'categorie'=>$categorie,
					'poza'=>$poza
			];
			array_push($prajituri,$obj);
		}
	
		$stmt->close();
	
		return $prajituri;
	}
	
	public static function getPretDupaIDPrajitura( $idPrajitura ){
		require("./util/DBConnector.php");
		
		$stmt = $conn->prepare("
			SELECT
			CASE 
				WHEN promo.id IS NOT NULL
				THEN promo.pret
				ELSE praji.pret
			END AS pret
			FROM prajituri praji
			LEFT JOIN promotie promo ON promo.id_prajitura = praji.id
			WHERE praji.id = ?");
		$stmt->bind_param("i", $idPrajitura);
		$stmt->execute();
		$stmt->bind_result($pret);
		if($stmt->fetch()){
			return $pret;
		}
	}
}

?>