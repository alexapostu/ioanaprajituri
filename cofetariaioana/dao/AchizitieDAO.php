<?php
class AchizitieDAO {
	public static function getAdreseDupaUtilizator($idUtilizator) {
		require ("./util/DBConnector.php");
		
		$adrese = [ ];
		$stmt = $conn->prepare ( "SELECT adresa, id FROM adrese WHERE id_utilizator = ?" );
		$stmt->bind_param ( "i", $idUtilizator );
		$stmt->execute ();
		$stmt->bind_result ( $adresa, $id );
		while ( $stmt->fetch () ) {
			$obj = ( object ) [ 
					'adresa' => $adresa,
					'id' => $id 
			];
			
			array_push ( $adrese, $obj );
		}
		$stmt->close ();
		
		return $adrese;
	}
	public static function inregistrareComanda($id_adresa, $prajituri) {
		require ("./util/DBConnector.php");
		
		$stmt = $conn->prepare ( "INSERT INTO comenzi(id_adresa) values(?)" );
		$stmt->bind_param ( "i", $id_adresa );
		$stmt->execute ();
		// Obtine ID-ul comenzii nou introduse
		$idComanda = $stmt->insert_id;
		$stmt->close ();
		$idProduse = array_keys ( $prajituri );
		foreach ( $idProduse as $idProdus ) {
			$produs = $prajituri [$idProdus];
			
			$prodStmt = $conn->prepare ( "INSERT INTO produse_comanda(id_prajitura, id_comanda, pret, numar_prajituri) values(?, ?, ?, ?)" );
			$prodStmt->bind_param ( "iidi", $idProdus, $idComanda, $produs->pret, $produs->cantitateProdusDeAdaugat);
			$prodStmt->execute ();
			$prodStmt->close ();
		}
	}
}
?>