<?php
class ComenziDAO {
	public static function getToateComenzile() {
		require ("./util/DBConnector.php");
		
		$comenzi = [ ];
		
		$stmt = $conn->prepare ( "
			SELECT com.id, adr.adresa, com.stare, u.login
			FROM comenzi com
			INNER JOIN adrese adr on com.id_adresa = adr.id
			INNER JOIN users u on u.id = adr.id_utilizator
			ORDER by com.stare ASC, com.id desc" );
		
		$stmt->execute ();
		$stmt->bind_result ( $id, $adresa, $stare, $login );
		while ( $stmt->fetch () ) {
			$obj = ( object ) [ 
					'adresa' => $adresa,
					'id' => $id,
					'stare' => $stare,
					'login' => $login
			];
			
			array_push ( $comenzi, $obj );
		}
		$stmt->close ();
		
		return $comenzi;
	}
	public static function getProduseComandate() {
		require ("./util/DBConnector.php");
		
		$produse = [ ];
		
		$stmt = $conn->prepare ( "SELECT 
			prod.id_prajitura, prod.id_comanda, prod.pret, prod.numar_prajituri, praji.nume, praji.poza
			FROM produse_comanda prod
			INNER JOIN prajituri praji on prod.id_prajitura = praji.id" );
		
		$stmt->execute ();
		$stmt->bind_result ( $id_prajitura, $id_comanda, $pret, $cantitate, $nume, $poza);
		while ( $stmt->fetch () ) {
			$obj = ( object ) [ 
					'id_prajitura' => $id_prajitura,
					'id_comanda' => $id_comanda,
					'pret' => $pret,
					'cantitate' => $cantitate,
					'nume' => $nume,
					'poza' => $poza
			];
			if (! isset ( $produse [$id_comanda] )) {
				$produse [$id_comanda] = [ ];
			}
			array_push ( $produse [$id_comanda], $obj );
		}
		$stmt->close ();
		
		return $produse;
	}
	
	public static function marcheazaLivrat($idComanda){
		require ("./util/DBConnector.php");
		
		$stmt = $conn->prepare ( "UPDATE comenzi set stare = 1 where id = ?" );
		$stmt->bind_param("i", $idComanda);
		
		$stmt->execute ();
		$stmt->close ();
	}
}
?>