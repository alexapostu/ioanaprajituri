<?php

class RegisterDAO{
	public static function existaUtilizator( $numeUtilizator ){
		require("./util/DBConnector.php");
		
		$stmt = $conn->prepare("SELECT id FROM users WHERE login = ?");
		$stmt->bind_param("s", $numeUtilizator);
		$stmt->execute();
		$stmt->bind_result($id);
		$stmt->fetch();
		$stmt->close();
		
		if($id != null){
			return true;
		}
		
		return false;
	}
	
	public static function inregistrareUtilizator( $numeUtilizator, $parola ){
		require("./util/DBConnector.php");
		
		$stmt = $conn->prepare("INSERT INTO users(login, password, type) VALUES(?, ?, 2)");
		$stmt->bind_param("ss", $numeUtilizator, $parola);
		$stmt->execute();
		$stmt->close();
		
		return false;
	}
}

?>