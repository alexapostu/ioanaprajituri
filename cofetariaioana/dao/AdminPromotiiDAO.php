<?php

class AdminPromotiiDAO{
	public static function getPromotieDupaId( $id){
		require("./util/DBConnector.php");
		
		$promotie = null;
		
		$stmt = $conn->prepare("SELECT  praji.nume, praji.descriere, promo.pret, praji.pret, praji.poza, praji.id 
		FROM promotie promo INNER JOIN prajituri praji ON promo.id_prajitura = praji.id
		WHERE promo.id = ?");
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt->bind_result($nume,$descriere, $pretNou, $pretVechi, $poza, $id_prajitura);
		$stmt->fetch();
		$stmt->close();
		
		if($id != null){
			$promotie= (object) [
					'id' => $id,
					'nume' => $nume,
					'descriere'=>$descriere,
					'pretNou'=>$pretNou,
					'pretVechi'=>$pretVechi,
					'poza'=>$poza,
					'id_prajitura'=>$id_prajitura
			];
		}
		
		return $promotie;
	}
	
	public static function updatePromotie($promotie){
		require("./util/DBConnector.php");
		$updateQuery = "UPDATE promotie SET pret = ?, id_prajitura = ? WHERE id = ?";
		$stmt = $conn->prepare( $updateQuery);
		
		$stmt->bind_param("dii", 
				$promotie->pret,
				$promotie->id_prajitura,
				$promotie->id);
		$stmt->execute();
		$stmt->close();
	}
	
	public static function addPromotie($promotie){
		require("./util/DBConnector.php");
		$insertQuery = "INSERT INTO promotie(pret, id_prajitura) VALUES(?, ?)";
		$stmt = $conn->prepare( $insertQuery);
		
		$stmt->bind_param("di",
				$promotie->pret,
				$promotie->id_prajitura);
		$stmt->execute();
		$stmt->close();
	}
	
	public static function stergePromotiaDupaID($id){
		require("./util/DBConnector.php");
		$deleteQuery = "DELETE FROM promotie WHERE id = ?";
		$stmt = $conn->prepare( $deleteQuery);
		
		$stmt->bind_param("i",$id);
		$stmt->execute();
		$stmt->close();
	}
}

?>