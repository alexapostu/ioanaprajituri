<?php

class AdaugareProdusDAO{
	public static function adaugaAdresa($adresa, $idUtilizator){
		require("./util/DBConnector.php");		
		$stmt = $conn->prepare("INSERT INTO adrese(id_utilizator, adresa) VALUES(?, ?)");
		$stmt->bind_param("is", $idUtilizator, $adresa);
		$stmt->execute();
	}
}

?>