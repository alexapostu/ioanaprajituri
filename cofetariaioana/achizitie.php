<?php
include 'util/GenericController.php';
include 'services/AchizitieService.php';

class AchizitieController extends GenericController {
	public static function getDefault() {
		$model = AchizitieService::getAdrese();
		include 'view/achizitie_view.php';
	}
	
	public static function getFinalizareComanda(){
		$model = AchizitieService::finalizareComanda();
		$model->adrese = AchizitieService::getAdrese()->adrese;
		include 'view/achizitie_view.php';
	}
}

AchizitieController::resolveAction ();
?>