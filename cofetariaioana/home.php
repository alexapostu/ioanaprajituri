<?php
require_once 'util/GenericController.php';
require_once 'services/HomeService.php';
require_once 'services/PromotieService.php';

class HomeController extends GenericController {
	public static function getDefault() {
		$model = HomeService::getMenus ();
		$model2 = PromotieService::getPromotiisiPrajituri ();
		
		include 'view/home_view.php';
	}
}

HomeController::resolveAction ();
?>