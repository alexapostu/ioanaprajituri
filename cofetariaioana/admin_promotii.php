<?php
require_once 'util/GenericController.php';
require_once 'services/AdminPromotiiService.php';
class AdminPromotiiController extends GenericController {
	public static function getDefault() {
		$model = AdminPromotiiService::getPromotiiSiPrajituri ();
		
		include 'view/admin_promotii_view.php';
	}
	public static function getEdit() {
		$model = AdminPromotiiService::getPromotieDupaId ();
		
		include 'view/admin_promotii_edit_view.php';
	}
	public static function getUpdate() {
		AdminPromotiiService::updatePromotie ();
		header ( 'Location: admin_promotii.php' );
	}
	public static function getAdd() {
		$model = AdminPromotiiService::newPromotieModel ();
		include 'view/admin_promotii_edit_view.php';
	}
	public static function getDelete() {
		AdminPromotiiService::stergePromotia ();
		header ( 'Location: admin_promotii.php' );
	}
}

AdminPromotiiController::resolveAction ();
?>