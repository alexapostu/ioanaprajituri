<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title>Cofetaria Ioana :: Login</title>
		<meta name="description" content="Login page Cofetaria Ioana">
		<meta name="keywords" content="Your,Keywords">
		<?php include 'components/common_head.php'?>
	</head>
	
	<body>
		<!-- Page Wrapper -->
		<div class="wrapper">
			<!-- Header Start -->
			<?php include 'components/header.php' ?>

			<!-- Main Content -->
			<div class="main-content">
				<div class="container">
					<br />
					<div class="row">
						<div class="col-md-2">
						</div>
						<div class="col-md-8 form-background">
							<?php 
							if(isset($model) ){
								if(isset($model->error)){
									echo '<div class="form-error">' . $model->error. '</div><br />';
								}
								if(isset($model->success)){
									echo '<div class="form-success">' . $model->success . '</div><br />';
								}
								
							}
							?>
							
							<form role="form" action="login.php" method="POST" id="loginForm">
								<input type="hidden" name="action" value="login"/>
								<div class="form-group">
									<!-- Form input -->
									<input class="form-control" type="text" id="uname" name="logare" placeholder="Nume utilizator" />
								</div>
								<div class="form-group">
									<!-- Form input -->
									<input class="form-control" type="password" id="upass" name="parola" placeholder="Parola" />
								</div>
								<!-- Form button -->
								<button class="btn btn-danger btn-sm" type="submit">Autentificare</button>&nbsp;
								<button class="btn btn-default btn-sm" type="reset">Reset</button>
							</form>
						</div>
						<div class="col-md-2">
						</div>
					</div>
				</div>
			</div><!-- / Main Content End -->
			
			
		</div><!-- / Wrapper End -->
		
		
		<!-- Scroll to top -->
		<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span> 
		<?php include 'components/scripts.php'; ?>
	</body>	
</html>