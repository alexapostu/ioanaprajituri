<!DOCTYPE html>
<html>
<head>
<!-- Title here -->
<title>Cofetaria Ioana :: Vizualizare promotii</title>
<meta name="description" content="Login page Cofetaria Ioana">
<meta name="keywords" content="Your,Keywords">
		<?php include 'components/common_head.php'?>
	</head>

<body>
	<!-- Page Wrapper -->
	<div class="wrapper">
		<?php include 'components/header.php' ?>

			<!-- Main Content -->
		<div class="main-content">
			<br />
			<div class="container form-background" style="min-height: 300px">
				<div class="col-md-12">
					<div class="row" style="text-align: right">
						<div class="col-md-10 col-xs-10"></div>
						<div class="col-md-2 col-xs-2">
							<a href="admin_promotii.php?action=add" class="btn btn-block btn-success">Adauga</a>
							<br/>
						</div>
					</div>
					<div class="row table-header">
						<div class="col-md-2 col-xs-2">
							Poza
						</div>
						<div class="col-md-2 col-xs-2">
							Nume
						</div>
						<div class="col-md-5 col-xs-5">
							Descriere
						</div>
						<div class="col-md-1 col-xs-1">
							Pret
						</div>
						<div class="col-md-2 col-xs-2">
							&nbsp;
						</div>
					</div>
					<?php 
					$numar_prajituri = count( $model->promotii );
					for($i = 0; $i < $numar_prajituri; $i++){
						$promotiiSiPrajituri = $model->promotii[ $i ];						
						
					?>
					
					<div class="row table-row">
						<div class="col-md-2 col-xs-2" style="padding: 0px;">
							<?php 
							if(	$promotiiSiPrajituri->poza != null){
								echo '<img class="img-responsive" src="img/prajituri/' . $promotiiSiPrajituri->poza . '" />';
							}
							?>
							
						</div>
						<div class="col-md-2 col-xs-2">
							<?php echo $promotiiSiPrajituri->nume ?>
						</div>
						<div class="col-md-5 col-xs-5">
							<?php echo $promotiiSiPrajituri->descriere ?>
						</div>
						<div class="col-md-1 col-xs-1">
							<?php echo $promotiiSiPrajituri->pretNou . ' LEI' ?>
						</div>
						<div class="col-md-2 col-xs-2">
							<a href="admin_promotii.php?action=edit&id=<?php echo $promotiiSiPrajituri->id ?>" class="btn btn-block btn-info">Modifica</a>
							<a href="admin_promotii.php?action=delete&id=<?php echo $promotiiSiPrajituri->id ?>" class="btn btn-block btn-danger btn-sm">Sterge</a>
						</div>
					</div>
					<?php 
					}
					?>
					
				</div>
			</div>
			<!-- / Main Content End -->

		</div>
		</div>
		<!-- / Wrapper End -->


		<!-- Scroll to top -->
		<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span>
		<?php include 'components/scripts.php'; ?>
		
</body>
</html>