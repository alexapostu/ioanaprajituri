<!DOCTYPE html>
<html>
<head>
<!-- Title here -->
<title>Cofetaria Ioana :: Modificare promotii</title>
<meta name="description" content="Login page Cofetaria Ioana">
<meta name="keywords" content="Your,Keywords">
		<?php include 'components/common_head.php'?>
	</head>

<body>
	<!-- Page Wrapper -->
	<div class="wrapper">
		<?php include 'components/header.php' ?>

			<!-- Main Content -->
		<div class="main-content">
			<br />
			<div class="container form-background" style="min-height: 300px">
				<div class="col-md-12">
					<form method="POST">
						<input type="hidden" value="update" name="action"/>
						<input type="hidden" value="<?php echo $model->id ?>" name="id"/>
						
						<div class="row row-margin-5">
							<div class="col-md-4 col-xs-4 align-right">
								Pret:
							</div>
							<div class="col-md-8 col-xs-8">
								<input type="number" class="form-control" value="<?php echo $model->pretNou ?>" name="pret" step="0.01"/>
							</div>
						</div>
						<div class="row row-margin-5">
							<div class="col-md-4 col-xs-4 align-right">
								Pret original:
							</div>
							<div class="col-md-8 col-xs-8">
								<input type="number" disabled="disabled" class="form-control" value="<?php echo $model->pretVechi ?>" name="pret" />
							</div>
						</div>
						<div class="row row-margin-5">
							<div class="col-md-4 col-xs-4 align-right">
								Prajitura:
							</div>
							<div class="col-md-8 col-xs-8">
								<select name="id_prajitura" class="form-control" >
									<?php 
									for($i = 0; $i < count( $model->prajituri); $i++){
										$prajitura = $model->prajituri[ $i ];
										$selected = '';
										if($prajitura->id == $model->id_prajitura){
											$selected = 'selected="selected"';
										}
										echo '<option ' . $selected . ' value="' . $prajitura->id. '">' . $prajitura->nume . '</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="row row-margin-5">
							<div class="col-md-4 col-xs-4 align-right"></div>
							<div class="col-md-8 col-xs-8">
								<input class="btn btn-success" type="submit" value="Salveaza" />
							</div>
						</div>
					</form>				
				</div>
			</div>
			<!-- / Main Content End -->
		</div>
		</div>
		<!-- / Wrapper End -->


		<!-- Scroll to top -->
		<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span>

		<?php include 'components/scripts.php'; ?>
</body>
</html>