<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<!-- Title here -->
		<title>Cofetaria Ioana :: Despre noi</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<?php include 'components/common_head.php' ?>
	</head>
	
	<body>
		<!-- Page Wrapper -->
		<div class="wrapper">
			<?php include 'components/header.php' ?>
			
			<!-- Banner Start -->
			
			<div class="banner banner-aboutus padd">
				<div class="container">
					<!-- Image -->
					<img class="img-responsive" src="img/crown-white.png" alt="" />
					<!-- Heading -->
					<h2 class="white">Despre noi</h2>
					<ol class="breadcrumb">
						<li><a href="home.php">Acasa</a></li>
						<li class="active">Despre noi</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
			
			<!-- Banner End -->
			
			
			
			<!-- Inner Content -->
			<div class="inner-page padd">
			
				<!-- About company -->
				<div class="about-company padd">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<!-- About Compnay Item -->
								<div class="about-company-item">
									<!-- About Company Image -->
									<img class="img-responsive img-thumbnail" src="img/chef/about-1.jpg" alt="" />
								</div>
							</div>
							<div class="col-md-6">
								<!-- About Compnay Item -->
								<div class="about-company-item">
									<!-- Heading -->
									<h3><span class="lblue">"Viata...este o bucata de prajitura!"</span></h3>
									<!-- Paragraph -->
									<p>COFETARIA IOANA s-a nascut in 2017 dintr-o pasiune pentru tot ce este bun, dulce si aduce zambete pe fetele oamenilor. Suntem pasionati de produsele naturale, proaspete, create zilnic de maestrii nostri cofetari si patiseri din ingrediente atent selectionate. In timp am castigat fidelitatea clientilor nostri si ne mandrim cu un numar considerabil de clienti multumiti. Ne dorim sa fim si mai aproape de voi, astfel acum puteti comanda online dulciurile zeilor, asa cum ne place sa le numim, precum si multe alte preparate gatite cu aceeasi pasiune.</p>
									<br /><!--/ Line break -->
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Chefs Start -->
				
				<div class="chefs padd">
					<div class="container">
						<!-- Default Heading -->
						<div class="default-heading">
							<!-- Crown image -->
							<img class="img-responsive" src="img/crown.png" alt="" />
							<!-- Heading -->
							<h2>Despre cofetarii nostri</h2>
							<!-- Paragraph -->
							<p>Cu o echipa tanara, de incredere si plina de energie suntem aici sa raspundem cerintelor tale cu entuziasm, promptitudine si profesionalism.</p>
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<!-- Chef Member -->
								<div class="chefs-member">
									<!-- Chefs member header -->
									<div class="chefs-head">
										<!-- Member background image -->
										<img class="chefs-back img-responsive" src="img/chef/c-back2.jpg" alt="" />
										<!-- chef member image -->
										<img class="chefs-img img-responsive" src="img/chef/4.jpg" alt="" />
									</div>
									<!--Name / Heading -->
									<h3><a href="#">Popescu Ioana</a></h3>
									<!-- Member designation -->
									<span>Sef Bucatar</span>
								</div>
							</div>
							<div class="col-md-4 col-sm-4">
								<!-- Chef Member -->
								<div class="chefs-member">
									<!-- Chefs member header -->
									<div class="chefs-head">
										<!-- Member background image -->
										<img class="chefs-back img-responsive" src="img/chef/c-back1.jpg" alt="" />
										<!-- chef member image -->
										<img class="chefs-img img-responsive" src="img/chef/7.jpg" alt="" />
									</div>
									<!--Name / Heading -->
									<h3><a href="#">Ionescu Andreea</a></h3>
									<!-- Member designation -->
									<span>Sef Bucatar</span>
								</div>
							</div>
							<div class="col-md-4 col-sm-4">
								<!-- Chef Member -->
								<div class="chefs-member">
									<!-- Chefs member header -->
									<div class="chefs-head">
										<!-- Member background image -->
										<img class="chefs-back img-responsive" src="img/chef/c-back3.jpg" alt="" />
										<!-- chef member image -->
										<img class="chefs-img img-responsive" src="img/chef/2.jpg" alt="" />
									</div>
									<!--Name / Heading -->
									<h3><a href="#">Georgescu Simona</a></h3>
									<!-- Member designation -->
									<span>Sef Bucatar</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Chefs End -->
			</div><!-- / Inner Page Content End -->	
			
		</div><!-- / Wrapper End -->
		
		
		<!-- Scroll to top -->
		<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span> 
		<?php include 'components/scripts.php'; ?>
	</body>	
</html>