<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<!-- Title here -->
		<title>Cofetaria Ioana :: Shopping</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<?php include 'components/common_head.php' ?>
	</head>
	
	<body>
		<?php include 'shopping_cart_modal.php'; ?>
		
		<!-- Page Wrapper -->
		<div class="wrapper">
			<?php include 'components/header.php' ?>
			
			<!-- Banner Start -->
			
			<div class="banner banner-shopping padd">
				<div class="container">
					<!-- Image -->
					<img class="img-responsive" src="img/crown-white.png" alt="" />
					<!-- Heading -->
					<h2 class="white">Shopping</h2>
					<ol class="breadcrumb">
						<li><a href="home.php">Acasa</a></li>
						<li class="active">Shopping</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
			
			<!-- Banner End -->
			
			
			
			<!-- Inner Content -->
			<div class="inner-page padd">
				
				<!-- Shopping Start -->
				
				<div class="shopping">
					<div class="container">
						<!-- Shopping items content -->
						<div class="shopping-content">
							<div class="row">
							<?php
							$prajituraEvidentiata = 0;
							if(isset( $model->evidentiat )){
								$prajituraEvidentiata = $model->evidentiat;
							}
						    for($i = 0; $i < count( $model->prajituri); $i++){
						    	$prajitura = $model->prajituri [$i];
						    	$evidentiat = $prajituraEvidentiata == $prajitura->id ? "evidentiat" : "";
						    ?>
								<div class="col-md-3 col-sm-6" style="position:relative; top :0px; bottom: 0px">
									<!-- Shopping items -->
									<div id="prajitura-<?php echo $prajitura->id ?>" class="shopping-item <?php echo $evidentiat?>" onclick="adaugaInCos(this)"
										nume="<?php echo $prajitura->nume ?>"
										pret="<?php echo $prajitura->pret ?>"
										idPrajitura="<?php echo $prajitura->id ?>">
										<!-- Image -->
										<div class="shopping-image">
											<img class="img-responsive" src="img/prajituri/<?php echo $prajitura->poza ?>" alt="" />
										</div>
										
										<!-- Shopping item name / Heading -->
										<span class="item-price pull-right">
											<?php echo $prajitura->pret ?>
										</span>
										<br />
										<h3>
											<?php echo $prajitura->nume ?>
										</h3>
										<div class="clearfix"></div>
										<!-- Paragraph -->
										<p> <?php echo $prajitura->descriere ?></p>
										<!-- Buy now button -->
										<div class="visible-xs">
											<a class="btn btn-danger btn-sm" href="#">Cumpara acum</a>
										</div>
										
										<!-- Shopping item hover block & link -->
										<div class="item-hover br-red hidden-xs"></div>
										<a class="link hidden-xs">Adauga in cos</a>
									</div>
								</div>
								<?php 
						}
							?>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Shopping End -->
				
			</div><!-- / Inner Page Content End -->				
		</div><!-- / Wrapper End -->
		
		
		<!-- Scroll to top -->
		<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span> 
		<?php include 'components/scripts.php'; ?>
		<script>

		// Functie pentru a face optiunile din lista de inaltime egala
		$(document).ready(function(){
			$('.shopping-item').matchHeight();
			$('.shopping-image').matchHeight();
		});
		
		</script>
		<script type="text/javascript">
		function adaugaInCos(prajitura){
			// Salveaza in variabile valorile numelui / pretului / id-ului prajiturii
			var numePrajitura = $(prajitura).attr('nume');
			var pretPrajitura = $(prajitura).attr('pret');
			var idPrajitura = $(prajitura).attr('idPrajitura');

			// Completeaza interfata modala cu informatii
			$('#numeProdusDeAdaugat').text( numePrajitura );
			$('#pretProdus').val( pretPrajitura );
			$('#pretTotal').text( pretPrajitura );
			$('[name="idPrajitura"]').val( idPrajitura );
			// Cantitatea initiala este 1
			$('#cantitateProdusDeAdaugat').val(1);

			// Afiseaza fereastra modala
			$('#modalAdaugareInCos').modal('show');

			calculPretTotal();
		}

		// Afiseaza pretul total bazat pe pretul unui produs inmultit cu numarul de produse
		function calculPretTotal(){
			var pretPrajitura = Number( $('#pretProdus').val() );
			var numarPrajituri = Number( $('#cantitateProdusDeAdaugat').val() );
			console.log('pret: ' + pretPrajitura + ", numarPrajituri: " + numarPrajituri);
			$('#pretTotal').text( pretPrajitura * numarPrajituri );
		}
		</script>
	</body>	
</html>