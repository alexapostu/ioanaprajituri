<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title>Cofetaria Ioana :: Meniu</title>
		<meta name="description" content="Meniu Cofetaria Ioana">
		<meta name="keywords" content="Your,Keywords">
		<?php include 'components/common_head.php' ?>
	</head>
	
	<body>
		
			
		<?php 
			include 'shopping_cart_modal.php';
		?>
		
		<!-- Page Wrapper -->
		<div class="wrapper">
		
			
			<?php include 'components/header.php' ?>
			<!-- Banner Start -->
			
			<div class="banner padd">
				<div class="container">
					<!-- Image -->
					<img class="img-responsive" src="img/crown-white.png" alt="" />
					<!-- Heading -->
					<h2 class="white">Meniu</h2>
					<ol class="breadcrumb">
						<li><a href="home.php">Acasa</a></li>
						<li class="active">Meniu</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
			
			<!-- Banner End -->
			
			
			
			<!-- Inner Content -->
			<div class="inner-page padd">
			
				<!-- Inner page menu start -->
				<div class="inner-menu">
					<div class="container">
						<div class="row">
						<?php 
						for($i = 0; $i < count( $model->categorii_prajituri); $i++){
							
							$categorie = $model->categorii_prajituri [$i];
						?>
							<div class="col-md-4 col-sm-6">
								<!-- Inner page menu list -->
								<div class="menu-list">
									<!-- Menu item heading -->
									<h3><?php echo $categorie->descriere ?></h3>
									<!-- Image for menu list -->
									<img class="img-responsive" src="img/menu/<?php echo $categorie->poza ?>" alt="" />
									
									<?php 
									for($j= 0; $j < count( $model->prajituri); $j++){
										
										$prajitura = $model->prajituri [$j];
										if($prajitura->categorie!=$categorie->id){
											continue;
										}
									
									?>
									
									
									
									<!-- Menu list items -->
									<a href="shopping.php?evidentiat=<?php echo $prajitura->id ?>/#prajitura-<?php echo $prajitura->id ?>">
										<div class="menu-list-item">
											<!-- Heading / Dish name -->
											<h4 class="pull-left"><?php echo $prajitura->nume ?></h4>
											<!-- Dish price -->
											<span class="price pull-right"><?php echo $prajitura->pret ?></span>
											<div class="clearfix"></div>
										</div>
									</a>
									<?php 
						                 }
							        ?>
								</div>
							</div>
							<?php 
						}
							?>
						</div>
					</div>
				</div>
				
				<!-- Inner page menu end -->
			
				
			</div><!-- / Inner Page Content End -->	
			
		</div><!-- / Wrapper End -->
		
		
		<!-- Scroll to top -->
		<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span> 
		<?php include 'components/scripts.php'; ?>
	</body>	
</html>