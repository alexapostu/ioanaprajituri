<!DOCTYPE html>
<html>
<head>
<!-- Title here -->
<title>Cofetaria Ioana :: Vizualizare promotii</title>
<meta name="description" content="Login page Cofetaria Ioana">
<meta name="keywords" content="Your,Keywords">
<?php include 'components/common_head.php'?>
<?php 

$utilizator = null;
if (isset ( $_SESSION ['user'] ) && $_SESSION ['user'] != null) {
	$utilizator = $_SESSION ['user'];
}

?>
</head>

<body>
	<?php include 'shopping_cart_modal.php'; ?>
	<!-- Page Wrapper -->
	<div class="wrapper">
		<?php include 'components/header.php' ?>

			<!-- Main Content -->
		<div class="main-content">
			<br />
			<div class="container form-background" style="min-height: 300px">
				<div class="col-md-12">
					<?php 
					if(isset($model) ){
						if(isset($model->errors)){
							for($i = 0; $i < count( $model->errors ); $i++){
								echo '<div class="form-error">' . $model->errors[$i] . '</div><br />';
							}
						}
						if(isset($model->success)){
							echo '<div class="form-success">' . $model->success. '</div><br />';
						}
					}
					
					?>
					<div class="row">
						<div class="col-md-9">
							<h2>Produse in comanda</h2>
						</div>
						<div class="col-md-3">
							<a href="#finalizareComanda" class="btn btn-block btn-info">Detalii livrare</a>
						</div>
					</div>
					<?php
					if (isset ( $_SESSION ['comanda'] ) && isset ( $_SESSION ['comanda']->prajituri )) {
						$idProduse = array_keys ( $_SESSION ['comanda']->prajituri );
						foreach ( $idProduse as $idProdus ) {
							$produs = $_SESSION ['comanda']->prajituri [$idProdus];
							?>
					
					<div class="row">
						<div class="col-md-2 col-xs-2" style="padding: 0px;">
							<?php
							if ($produs->poza != null) {
								echo '<img class="img-responsive" src="img/prajituri/' . $produs->poza . '" />';
							}
							?>
							
						</div>
						<div class="col-md-6 col-xs-6">
							<div>
								<h3><?php echo $produs->nume ?></h3>
							</div>
							<div style="font-size: 14px"><?php echo $produs->descriere ?></div>
						</div>
						<div class="col-md-2 col-xs-2">
							<span style="font-size: 18px" class="align-middle"><?php echo $produs->cantitateProdusDeAdaugat ?> buc</span>
						</div>
						<div class="col-md-1 col-xs-1">
							<span style="font-size: 18px" class="align-middle"><?php echo ($produs->cantitateProdusDeAdaugat * $produs->pret ) . ' LEI' ?></span>
						</div>
						<div class="col-md-1 col-xs-1">
							<a class="btn btn-block btn-danger" href="adaugare_produs.php?action=stergeProdus&idPrajitura=<?php echo $produs->id ?>">x</a>
						</div>
					</div>
					<hr />
					<?php
						}
					}
					if(!isset ( $_SESSION ['comanda'] ) 
							|| !isset ( $_SESSION ['comanda']->prajituri )
							|| count( $_SESSION ['comanda']->prajituri ) == 0 ){
						echo '<span style="color: red">Nu aveti niciun produs in cos!</span>';
					}
					?>
					<div class="row">
						<div class="col-md-12">
							<h2 id="finalizareComanda">Finalizare comanda</h2>
						</div>
					</div>
					<?php 
						if( $utilizator == null ){
					?>
					<div class="row">
						<div class="col-md-12">
							<div class="auth-required">Pentru a finaliza comanda trebuie sa va autentificati!</div>
						</div>
					</div>
					<div class="row" style="min-height: 300px">
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<a href="login.php" class="btn btn-block btn-info">Utilizator existent</a>
						</div>
						<div class="col-md-3">
							<a href="register.php" class="btn btn-block btn-info">Utilizator nou</a>
						</div>
						<div class="col-md-3"></div>
					</div>
					<?php
						}else{
					?>
					<div class="row">
						<div class="col-md-9">
							<h3>Alegeti adresa de livrare:</h3>
						</div>
						<div class="col-md-3">
							<a onclick="adaugareAdresa()" class="btn btn-block btn-info">Adaugare adresa</a>
						</div>
					</div>
					<form action="achizitie.php" method="POST">
						<input type="hidden" name="action" value="finalizareComanda"/>
						<div class="row">
							<div class="col-md-12">					
								<?php 
								if( $model->adrese == null || count( $model->adrese) == 0 ){
									echo '<span style="color: red">Nu aveti nicio adresa adaugata!</span>';	
								}else{
									$numarAdrese = count( $model->adrese );
									for( $i = 0; $i < $numarAdrese; $i++ ){
										$adresa = $model->adrese[ $i ];
								?>
										<div class="row">
											<div class="col-md-12 adrese-list-row-container">
												<h4>
													<input type="radio" name="adresa" value="<?php echo $adresa->id; ?>" /><span class="address-list-item"><?php echo $adresa->adresa?></span>
												</h4>
											</div>
										</div>		
								<?php 
										
									}
								}
								?>
							</div>
							
						</div>
						<?php 
							}
						?>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<br />
								<input type="submit" class="btn btn-block btn-success" value="Finalizare comanda" />
							</div>
							<div class="col-md-4"></div>
						</div>
					</form>
						
				</div>
			</div>
			<!-- / Main Content End -->

		</div>
	</div>
	<!-- / Wrapper End -->


	<!-- Scroll to top -->
	<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span>



	<?php include 'components/scripts.php'; ?>
	<script type="text/javascript">
	function adaugareAdresa(){
		$('#modalAdaugareAdresa').modal('show');
	}
	</script>
</body>
</html>