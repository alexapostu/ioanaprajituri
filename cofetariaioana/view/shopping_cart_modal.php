<!-- Adaugare in cos -->
<div class="modal fade" id="modalAdaugareInCos" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Adauga in cos</h4>
			</div>
			
			<form action="adaugare_produs.php" method="POST">
				<input type="hidden" id="idPrajitura" name="idPrajitura"/>
				<input type="hidden" id="pretProdus" name="pretProdus"/>
				<div class="modal-body">
				<!-- Items table -->
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Nume</th>
								<th>Cantitate</th>
								<th>Pret</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><span id="numeProdusDeAdaugat">Amandina</span></td>
								<td><input onchange="calculPretTotal()" type="number" value="1" name="cantitateProdusDeAdaugat" id="cantitateProdusDeAdaugat" /></td>
								<td><span id="pretTotal">12</span><span>RON</span></td>
							</tr>
						</tbody>
					</table>
				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-error" data-dismiss="modal">Anuleaza</button>
					<input type="submit" class="btn btn-info" value="Adauga la comanda" />
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Model End -->
<!-- Adaugare in cos -->
<div class="modal fade" id="modalAdaugareAdresa" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Adaugare adresa</h4>
			</div>
			
			<form action="adaugare_produs.php" method="POST">
				<input type="hidden" value="adaugaAdresa" name="action"/>
				<div class="modal-body">
				<span>Introduceti adresa la care doriti sa se faca livrarea:</span>
				<input type="text" name="adresa" class="form-control" />
				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-error" data-dismiss="modal">Anuleaza</button>
					<input type="submit" class="btn btn-info" value="Adauga adresa" />
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Model End -->