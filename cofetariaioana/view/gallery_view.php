<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title>Cofetaria Ioana :: Galerie</title>
		<meta name="description" content="Galerie Cofetaria Ioana">
		<meta name="keywords" content="Your,Keywords">
		<?php include 'components/common_head.php' ?>
	</head>
	
	<body>
		
		<?php include 'shopping_cart_modal.php'; ?>
		
		<!-- Page Wrapper -->
		<div class="wrapper">
		
			<!-- Header Start -->
			<?php include 'components/header.php' ?>
			<!-- Header End -->
			
			<!-- Banner Start -->
			
			<div class="banner banner-gallery padd">
				<div class="container">
					<!-- Image -->
					<img class="img-responsive" src="img/crown-white.png" alt="" />
					<!-- Heading -->
					<h2 class="white">Galerie</h2>
					<ol class="breadcrumb">
						<li><a href="home.php">Acasa</a></li>
						<li class="active">Galerie</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
			
			<!-- Banner End -->
			
			
			
			<!-- Inner Content -->
			<div class="inner-page padd">
				
				<!-- Gallery Start -->
				
				<div class="gallery">
					<div class="container">
						<!-- Gallery elements with pretty photo -->
						<div class="gallery-content">
							<div class="row">
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="img/dish/dish1.jpg" alt=""/>
										<!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="img/dish/dish1.jpg" class="gallery-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="img/dish/dish2.jpg" alt=""/>
										<!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="img/dish/dish2.jpg" class="gallery-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="img/dish/dish3.jpg" alt=""/>
										<!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="img/dish/dish3.jpg" class="gallery-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="img/dish/dish4.jpg" alt=""/>
										<!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="img/dish/dish4.jpg" class="gallery-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="img/dish/dish12.jpg" alt=""/>
										<!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="img/dish/dish12.jpg" class="gallery-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="img/dish/dish13.jpg" alt=""/>
										<!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="img/dish/dish13.jpg" class="gallery-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="img/dish/dish14.jpg" alt=""/>
										<!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="img/dish/dish14.jpg" class="gallery-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="img/dish/dish15.jpg" alt=""/>
										<!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="img/dish/dish15.jpg" class="gallery-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="img/dish/dish16.jpg" alt=""/>
										<!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="img/dish/dish16.jpg" class="gallery-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="img/dish/dish17.jpg" alt=""/>
										<!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="img/dish/dish17.jpg" class="gallery-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="img/dish/dish18.jpg" alt=""/>
										<!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="img/dish/dish18.jpg" class="gallery-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<!-- Separate gallery element -->
									<div class="element">
										<!-- Image -->
										<img class="img-responsive img-thumbnail" src="img/dish/dish19.jpg" alt=""/>
										<!-- Gallery Image Hover Effect -->
										<span class="gallery-img-hover"></span>
										<!-- Gallery Image Hover Icon -->
										<a href="img/dish/dish19.jpg" class="gallery-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
								</div>
							</div>
						</div><!-- Separate gallery element --><!--/ End Gallery content class -->
					</div>
				</div>
				
				<!-- Gallery End -->
			
				
			</div><!-- / Inner Page Content End -->	
			
			
		</div><!-- / Wrapper End -->
		
		
		<!-- Scroll to top -->
		<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span> 
		<?php include 'components/scripts.php'; ?>
		<script>
		/* Gallery */

		jQuery(".gallery-img-link").prettyPhoto({
		   overlay_gallery: false, social_tools: false
		});
		
		</script>
	</body>	
</html>