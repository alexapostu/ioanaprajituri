<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title>Cofetaria Ioana :: Acasa</title>
		<meta name="description" content="Home page Cofetaria Ioana">
		<meta name="keywords" content="Your,Keywords">
		<?php include 'components/common_head.php' ?>
	</head>
	
	<body>
		
		<?php include 'shopping_cart_modal.php'; ?>

		<!-- Page Wrapper -->
		<div class="wrapper">
			<!-- Header Start -->
			<?php include 'components/header.php' ?>
			<?php include 'components/home_carousel.php' ?>
	
				<!-- Dishes Start -->
					
				<div class="dishes padd">
					<div class="container">
						<!-- Default Heading -->
						<div class="default-heading">
							<!-- Crown image -->
							<img class="img-responsive" src="img/crown.png" alt="" />
							<!-- Heading -->
							<h2>Tipuri de prajituri noi</h2>
							<!-- Paragraph -->
							<p>Pentru a putea savura deliciile noi, iata o colectie de prajituri pe care te sfatuim sa le incerci.</p>
							<!-- Border -->
							<div class="border"></div>
						</div>
						<div class="row">
						
						
						
						<?php 
							$numar_prajituri = count($model->prajituri);
							if($numar_prajituri > 4){
								$numar_prajituri = 4;
							}
						    for($i = 0; $i < $numar_prajituri; $i++){
							
							    $prajitura = $model->prajituri [$i];
							    
						    ?>
							<div class="col-md-3 col-sm-6">	
								<div class="dishes-item-container">
									<!-- Image Frame -->
									<div class="prajituri-noi">
										<div class="img-frame">
											<!-- Image -->
											<img src="img/prajituri/<?php echo $prajitura->poza ?>" class="img-responsive" alt="" />
											<!-- Block for on hover effect to image -->
											<div class="img-frame-hover">
												<!-- Hover Icon -->
												<a href="shopping.php?evidentiat=<?php echo $prajitura->id ?>/#prajitura-<?php echo $prajitura->id ?>"><i class="fa fa-cutlery"></i></a>
											</div>
										</div>
										<!-- Dish Details -->
										<div class="dish-details">
											<!-- Heading -->
											<h3><a href="#"><h3><?php echo $prajitura->nume ?></h3></a></h3>
											<!-- Paragraph -->
											<p><?php echo $prajitura->descriere ?></p>
										</div>
									</div>
									<div style="text-align: center">
										<!-- Button -->
										<a href="shopping.php?evidentiat=<?php echo $prajitura->id ?>/#prajitura-<?php echo $prajitura->id ?>" class="btn btn-danger btn-sm">Cumpara</a>
									</div>
								</div>
							</div>
									<?php 
						}
							?>
						</div>
					</div>					
				</div>
				
				<!-- Dishes End -->
				
				<!-- menu Start -->
				
				<div class="menu padd">
					<div class="container">
						<!-- Default Heading -->
						<div class="default-heading">
							<!-- Crown image -->
							<img class="img-responsive" src="img/crown.png" alt="" />
							<!-- Heading -->
							<h2>CAKE BAR</h2>
							<!-- Paragraph -->
							<p>Fiecare dintre voi a avut nevoie rapid de ceva dulce la petrecerea sa privata. Acum noi iti venim in ajutor. Alege prajituri de calitate.</p>
							<!-- Border -->
							<div class="border"></div>
						</div>
						<!-- Menu content container -->
						<div class="menu-container">
							<div class="row">
							<?php 
						    for($i = 0; $i < count( $model->categorii_prajituri); $i++){
							
							    $categorie = $model->categorii_prajituri [$i];
						    ?>
								<div class="col-md-4 col-sm-4">
									<!-- Menu header -->
									<div class="menu-head">
										<!-- Image for menu item -->
										<img class="img-responsive" src="img/menu/<?php echo $categorie->poza ?>" alt="" />
										<!-- Menu title / Heading -->
										<h3><?php echo $categorie->descriere ?></h3>
										<?php 
									    for($j= 0; $j < count( $model->prajituri); $j++){
										
										$prajitura = $model->prajituri [$j];
										if($prajitura->categorie != $categorie->id){
											continue;
										}
									
									    ?>
										<!-- Border for above heading -->
										<div class="title-border br-red"></div>
									</div>
									<!-- Menu item details -->
									<div class="menu-details br-red">
										<!-- Menu list -->
										<ul class="list-unstyled">
											<li>
												<div class="menu-list-item">
													<!-- Icon -->
													<i class="fa fa-angle-right"></i> <?php echo $prajitura->nume ?>
													<!-- Price badge -->
													<span class="pull-right"><?php echo $prajitura->pret ?></span>
													<div class="clearfix"></div>
												</div>
												<?php 
						                        }
							                    ?>
											</li>
										</ul>
									</div><!-- / Menu details end -->
								</div>
								<?php 
						}
							?>
								
						</div>
						</div> <!-- /Menu container end -->
					</div>
				</div>
				
				<!-- Menu End -->
				
				<!-- Pricing Start -->
				
				<div class="pricing padd">
					<div class="container">
						<!-- Default Heading -->
						<div class="default-heading">
							<!-- Crown image -->
							<img class="img-responsive" src="img/crown.png" alt="" />
							<!-- Heading -->
							<h2>Promotii</h2>
							<!-- Paragraph -->
							<p>Facute in casa, retetele noastre sunt complet naturale!</p>
							<!-- Border -->
							<div class="border"></div>
						</div>
						<div class="row">
							<?php 
								$numar_promotii = count( $model2->promotiiSiPrajituri);
								if($numar_promotii > 4){
									$numar_promotii = 4;
								}
							    for($i = 0; $i < $numar_promotii; $i++){
							
							    	$promotie = $model2->promotiiSiPrajituri[$i];
							?>
							<div class="col-md-6 col-sm-6">
								<!-- Pricing list Item -->
								<div class="pricing-item">
									<!-- Image -->
									<a href="#"><img class="img-responsive img-thumbnail" src="img/prajituri/<?php echo $promotie->poza ?>" alt="" /></a>
									<!-- Pricing item details -->
									<div class="pricing-item-details">
										<!-- Heading -->
										<h3><a href="#"><h3><?php echo $promotie->nume ?></h3></a></h3>
										
										
										<!-- Paragraph -->
										<p> <?php echo $promotie->descriere ?></p>
										<br />
										<a class="btn btn-danger pull-right" href="shopping.php?evidentiat=<?php echo $promotie->id_prajitura ?>/#prajitura-<?php echo $promotie->id_prajitura?>">Cumpara</a>
										<div class="clearfix"></div>
									</div>
									<!-- Tag -->
									<span class="hot-tag br-red"><?php echo round( ($promotie->pretNou - $promotie->pretVechi)*100.0 /$promotie->pretVechi) ?>%</span>
									<div class="clearfix"></div>
									
								</div>
								
							</div>
							<?php 
	                        	}
		                    ?>
						
						</div>
					</div>
				</div>
				
				<!-- Pricing End -->
				
				
				
				
				
				
			</div><!-- / Main Content End -->	
			
			
		</div><!-- / Wrapper End -->
		
		
		<!-- Scroll to top -->
		<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span> 
		
		
		
		<?php include 'components/scripts.php'; ?>
		<script>
		/* ******************************************** */
		/*  JS for SLIDER REVOLUTION  */
		/* ******************************************** */
				jQuery(document).ready(function() {
					   jQuery('.tp-banner').revolution(
						{
							delay:9000,
							startheight:500,
							
							hideThumbs:10,
							
							navigationType:"bullet",	
														
							hideArrowsOnMobile:"on",
							
							touchenabled:"on",
							onHoverStop:"on",
							
							navOffsetHorizontal:0,
							navOffsetVertical:20,
							
							stopAtSlide:-1,
							stopAfterLoops:-1,

							shadow:0,
							
							fullWidth:"on",
							fullScreen:"off"
						});
				});
		/* ******************************************** */
		/*  JS for FlexSlider  */
		/* ******************************************** */
		
			$(window).load(function(){
				$('.flexslider-recent').flexslider({
					animation:		"fade",
					animationSpeed:	1000,
					controlNav:		true,
					directionNav:	false
				});
				$('.flexslider-testimonial').flexslider({
					animation: 		"fade",
					slideshowSpeed:	5000,
					animationSpeed:	1000,
					controlNav:		true,
					directionNav:	false
				});
			});		
		</script>
		<!-- JS code for this page -->
		<script type="text/javascript">

		// Functie pentru a face optiunile din lista de inaltime egala
		$(document).ready(function(){
			$('.prajituri-noi').matchHeight();
		});
		</script>
	</body>	
</html>