<!DOCTYPE html>
<html>
<head>
<!-- Title here -->
<title>Cofetaria Ioana :: Vizualizare prajituri</title>
<meta name="description" content="Login page Cofetaria Ioana">
<meta name="keywords" content="Your,Keywords">
		<?php include 'components/common_head.php'?>
	</head>

<body>
	<!-- Page Wrapper -->
	<div class="wrapper">
		<?php include 'components/header.php' ?>

			<!-- Main Content -->
		<div class="main-content">
			<br />
			<div class="container form-background" style="min-height: 300px">
				<div class="col-md-12">
					<h2>Comenzi</h2>
					
					<?php 
					$numar_comenzi = count( $model->comenzi );
					for($i = 0; $i < $numar_comenzi; $i++){
						$comanda = $model->comenzi[ $i ];
						$totalComanda = 0;
					
					?>
					<div class="row comanda-row">
						<div class="col-md-12 col-xs-12">
							<div class="row">
								<div class="col-md-1 col-xs-1">
									Comanda #<?php echo $comanda->id ?>
								</div>
								<div class="col-md-4 col-xs-4">
									<b>Adresa: </b><?php echo $comanda->adresa ?>
								</div>
								<div class="col-md-5 col-xs-5">
									<b>Nume: </b><?php echo $comanda->login ?>
								</div>
								<div class="col-md-2 col-xs-2">
									<?php 
									if( $comanda->stare == 1 ){
										echo '<b>Livrat</b>';
									}else{
										echo '<a href="comenzi.php?action=marcheazaLivrat&id=' . $comanda->id . '" class="btn btn-block btn-info" >Marcheaza livrat</a>';
									}
									?>
								</div>
							</div>
							<?php
								$numar_produse = count( $model->produse[ $comanda->id ] );
								for($j = 0; $j < $numar_produse; $j++){
									$produs = $model->produse[ $comanda->id ][ $j ];
									$totalComanda += $produs->pret * $produs->cantitate;
								?>
								<div class="row produs-row">
									<div class="col-md-1 col-xs-1"></div>
									<div class="col-md-1 col-xs-1">
										<?php 
										if(	$produs->poza != null){
											echo '<img class="img-responsive" src="img/prajituri/' . $produs->poza . '" />';
										}
										?>
										
									</div>
									<div class="col-md-4 col-xs-4">
										<?php echo $produs->nume ?>
									</div>
									<div class="col-md-2 col-xs-2">
										<?php echo $produs->cantitate ?> buc
									</div>
									<div class="col-md-2 col-xs-2">
										<?php echo $produs->pret ?>LEI / buc
									</div>
									<div class="col-md-2 col-xs-2">
										<?php echo $produs->pret * $produs->cantitate?> LEI total
									</div>
								</div>
							<?php
								}
							?>
							<div class="row" style="border-top : 1px black solid">
								<div class="col-md-8 col-xs-8"></div>
								<div class="col-md-2 col-xs-2">
									<b>Total comanda:</b>
								</div>
								<div class="col-md-2 col-xs-2">
									<b><?php echo $totalComanda?> LEI</b>
								</div>
							</div>
							
						</div>
					</div><br/>
					<?php
						}
					?>
				</div>
			</div>
			<!-- / Main Content End -->

			</div>
		</div>
		<!-- / Wrapper End -->


		<!-- Scroll to top -->
		<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span>
		<?php include 'components/scripts.php'; ?>
</body>
</html>