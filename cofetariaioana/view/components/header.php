<?php
$utilizator = null;
if (isset ( $_SESSION ['user'] ) && $_SESSION ['user'] != null) {
	$utilizator = $_SESSION ['user'];
}
$_SESSION ['url'] = $_SERVER ['REQUEST_URI'];
?>
<div class="header">
	<div class="container">
		<!-- Header top area content -->
		<div class="header-top">
			<div class="row">
				<div class="col-md-2 col-sm-2">
					<!-- Header top left content contact -->
					<div class="header-contact">
						<!-- Contact number -->
						<?php
						if ($utilizator != null) {
							echo $utilizator->login;
							echo "&nbsp;|&nbsp;";
							echo '<a href="logout.php">logout</a>';
						} else {
							echo '<a href="login.php">login</a>';
							echo "&nbsp;|&nbsp;";
							echo '<a href="register.php">inregistrare</a>';
						}
						?>
						
					</div>
				</div>
				<div class="col-md-2 col-sm-2">
					<div class="header-contact">
						<!-- Contact number -->
						<span class="pull-right"><i class="fa fa-phone red"></i> 0268 362 436 </span>
					</div>
				</div>
				<div class="col-md-2 col-sm-2">
					<div class="header-contact" style="text-align:center">
						<!-- Contact number -->
						<span><i class="fa fa-paper-plane red"></i> contact@ioana.ro </span>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="header-contact">
						<!-- Contact number -->
						<span class="pull-left"><i class="fa fa-map-marker red"></i> Str. Castanilor, nr5,
							Brasov </span>
					</div>
				</div>


				<div class="col-md-3 col-sm-3">
					<!-- Button Kart -->
					<div class="btn-cart-md">
						<a class="cart-link" href="#"> <!-- Image --> <img
							class="img-responsive" src="img/cart.png" alt=""> <!-- Heading -->
							<h4>Cos de cumparaturi</h4> <span></span>
							<div class="clearfix"></div>
						</a>
						<ul class="cart-dropdown" role="menu">
							<?php
							if (isset ( $_SESSION ['comanda'] ) && isset ( $_SESSION ['comanda']->prajituri )) {
								$idProduse = array_keys ( $_SESSION ['comanda']->prajituri );
								foreach ( $idProduse as $idProdus ) {
									$produs = $_SESSION ['comanda']->prajituri [$idProdus];
									
									?>
							
							<li>
								<div class="cart-item">
									<div class="row">
										<div class="col-md-3">
											<img class="img-responsive img-rounded"
												src="img/prajituri/<?php echo $produs->poza ?>" alt="" />
										</div>
										<div class="col-md-4">
											<div>&nbsp;<?php echo $produs->nume ?><br />
											</div>
										</div>
										<div class="col-md-3">
											<div class="red">
												<?php echo $produs->pret * $produs->cantitateProdusDeAdaugat?>RON
											</div>
										</div>
										<div class="col-md-2">
											<a style="cursor: pointer;" class="fa fa-times"></a>
										</div>
									</div>
								</div>
							</li>
							<?php
								}
							}
							?>
							<li>
								<!-- Cart items for shopping list -->
								<div class="cart-item">
									<a class="btn btn-danger" href="achizitie.php">Finalizare
										comanda</a>
								</div>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-5">
				<!-- Link -->
				<a href="home.php"> <!-- Logo area -->
					<div>
						<img class="img-responsive" src="img/logo_cofetaria_ioana.PNG"
							alt="" />
					</div>
				</a>
			</div>
			<div class="col-md-8 col-sm-7">
				<!-- Navigation -->
				<nav class="navbar navbar-default navbar-right" role="navigation">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse"
								data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span> <span
									class="icon-bar"></span> <span class="icon-bar"></span> <span
									class="icon-bar"></span>
							</button>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse"
							id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="home.php"><img src="img/nav-menu/nav1.jpg"
										class="img-responsive" alt="" /> Acasa </a></li>
								<li><a href="menu.php"><img src="img/nav-menu/nav2.jpg"
										class="img-responsive" alt="" /> Meniu </a></li>

								<li><a href="gallery.php"><img src="img/nav-menu/nav3.jpg"
										class="img-responsive" alt="" /> Galerie</a></li>
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown"><img src="img/nav-menu/nav4.jpg"
										class="img-responsive" alt="" /> Magazin <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li><a href="shopping.php">Shopping</a></li>
										<li><a href="achizitie.php">Finalizare comanda</a></li>
									</ul></li>
								<li><a href="aboutus.php"><img src="img/nav-menu/nav6.jpg"
										class="img-responsive" alt="" /> Despre noi</a></li>
										
								<?php
								if ($utilizator != null && $utilizator->type == 1) {
									?>
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown"><img src="img/nav-menu/nav5.png"
										class="img-responsive" alt="" />Administrator <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li><a href="admin_prajituri.php">Prajituri</a></li>
										<li><a href="admin_promotii.php">Promotii</a></li>

									</ul></li>
								<?php
								}
								?>
								
								<?php
								if ($utilizator != null && $utilizator->type == 3) {
									?>
								<li><a href="comenzi.php"><img src="img/nav-menu/nav7.jpg"
										class="img-responsive" alt="" /> Comenzi</a></li>
								<?php
								}
								?>
							</ul>
						</div>
						<!-- /.navbar-collapse -->
					</div>
					<!-- /.container-fluid -->
				</nav>
			</div>
		</div>
	</div>
	<!-- / .container -->
</div>