<meta charset="utf-8">
<!-- Description, Keywords and Author -->
<meta name="author" content="Ioana Despu">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Styles -->
<!-- Bootstrap CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link href="css/settings.css" rel="stylesheet">		
<!-- FlexSlider Css -->
<link rel="stylesheet" href="css/flexslider.css" media="screen" />
<!-- Portfolio CSS -->
<link href="css/prettyPhoto.css" rel="stylesheet">
<!-- Font awesome CSS -->
<link href="css/font-awesome.min.css" rel="stylesheet">	
<!-- Custom Less -->
<link href="css/less-style.css" rel="stylesheet">	
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<link href="css/page-style.css" rel="stylesheet">
<!--[if IE]><link rel="stylesheet" href="css/ie-style.css"><![endif]-->

<!-- Favicon -->
<link rel="shortcut icon" href="#">

