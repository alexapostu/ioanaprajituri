
<!-- Slider Start 
#################################
	- THEMEPUNCH BANNER -
#################################	-->

<div class="tp-banner-container">
	<div class="tp-banner" >
		<ul>	<!-- SLIDE  -->
			<li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
				<!-- MAIN IMAGE -->
				<img src="img/slideShow/slide.png"  alt=""  data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat">
				
				<!-- LAYERS -->
				<!-- LAYER NR. 1 -->
				<div class="tp-caption lfl largeblackbg br-red"
					data-x="20" 
					data-y="100"
					data-speed="1500"
					data-start="1200"
					data-easing="Power4.easeOut"
					data-endspeed="500"
					data-endeasing="Power4.easeIn"
					style="z-index: 3">Prajituri delicioase...
				</div>
				<!-- LAYER NR. 2.0 -->
				<div class="tp-caption lfl medium_bg_darkblue br-green"
					data-x="20"
					data-y="200"
					data-speed="1500"
					data-start="1800"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Power4.easeIn"
					data-captionhidden="off">Prajituri de dulce
				</div>dsfg
				<!-- LAYER NR. 2.1 -->
				<div class="tp-caption lfl medium_bg_darkblue br-lblue"
					data-x="20" 
					data-y="250"
					data-speed="1500"
					data-start="2100"
					data-easing="Power4.easeOut"
					data-endspeed="500"
					data-endeasing="Power4.easeIn"
					style="z-index: 3">Prajituri de post
				</div>
				<!-- LAYER NR. 2.2 -->
				<div class="tp-caption lfl medium_bg_darkblue br-purple"
					data-x="20" 
					data-y="300"
					data-speed="1500"
					data-start="2400"
					data-easing="Power4.easeOut"
					data-endspeed="500"
					data-endeasing="Power4.easeIn"
					style="z-index: 3">Prajituri Raw Vegane
				</div>
				<!-- LAYER NR. 2.3 -->
				<div class="tp-caption lfl medium_bg_darkblue br-orange"
					data-x="20" 
					data-y="350"
					data-speed="1500"
					data-start="2700"
					data-easing="Power4.easeOut"
					data-endspeed="500"
					data-endeasing="Power4.easeIn"
					style="z-index: 3">Prajituri pentru diabetici
				</div>
				<!-- LAYER NR. 3.0 -->
				<div class="tp-caption customin customout"
					data-x="right" data-hoffset="-50"
					data-y="100"
					data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="400"
					data-start="3600"
					data-easing="Power3.easeInOut"
					data-endspeed="300"
					style="z-index: 5"><img class="slide-img img-responsive" src="img/slideShow/s21.jpg" alt="" />
				</div>
				<!-- LAYER NR. 3.1 -->
				<div class="tp-caption customin customout"
					data-x="right" data-hoffset="-120"
					data-y="130"
					data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="400"
					data-start="3900"
					data-easing="Power3.easeInOut"
					data-endspeed="300"
					style="z-index: 6"><img class="slide-img img-responsive" src="img/slideShow/s22.jpg" alt="" />
				</div>
				<!-- LAYER NR. 3.2 -->
				<div class="tp-caption customin customout"
					data-x="right" data-hoffset="-10"
					data-y="160"
					data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="400"
					data-start="4200"
					data-easing="Power3.easeInOut"
					data-endspeed="300"
					style="z-index: 7"><img class="slide-img img-responsive" src="img/slideShow/s23.jpg" alt="" />
				</div>
				<!-- LAYER NR. 3.3 -->
				<div class="tp-caption customin customout"
					data-x="right" data-hoffset="-80"
					data-y="190"
					data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="400"
					data-start="4500"
					data-easing="Power3.easeInOut"
					data-endspeed="300"
					style="z-index: 8"><img class="slide-img img-responsive" src="img/slideShow/s24.jpg" alt="" />
				</div>
			</li>
			<li data-transition="zoomin" data-slotamount="6" data-masterspeed="400" >
				<!-- MAIN IMAGE -->
				<img src="img/slideShow/slide2.png" style="background-color:#fff" alt=""  data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat">
				
				<!-- LAYERS -->
				<!-- LAYER NR. 1 -->
				<div class="tp-caption sfl modern_medium_light"
					data-x="20" 
					data-y="90"
					data-speed="800"
					data-start="1000"
					data-easing="Power4.easeOut"
					data-endspeed="500"
					data-endeasing="Power4.easeIn"
					style="z-index: 3">Ocazii Speciale
				</div>
				<!-- LAYER NR. 1.1 -->
				<div class="tp-caption large_bold_grey heading customin customout"
					data-x="10"
					data-y="125"
					data-splitin="chars"
					data-splitout="chars"
					data-elementdelay="0.05"
					data-start="1500"
					data-speed="900"
					data-easing="Back.easeOut"
					data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-endspeed="500"
					data-endeasing="Power3.easeInOut"
					data-captionhidden="on"
					style="z-index:5">Cadouri
				</div>
				<!-- LAYER NR. 2 -->
				<div class="tp-caption customin customout"
					data-x="right" data-hoffset="-50"
					data-y="60"
					data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="800"
					data-start="2000"
					data-easing="Power4.easeOut"
					data-endspeed="500"
					data-endeasing="Power4.easeIn"
					style="z-index: 3"><img class="slide-img img-responsive" src="img/slideShow/s11.jpg" alt="" />
				</div>
				<!-- LAYER NR. 2.1 -->
				<div class="tp-caption customin customout"
					data-x="right" data-hoffset="-100"
					data-y="100"
					data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="800"
					data-start="2300"
					data-easing="Power4.easeOut"
					data-endspeed="500"
					data-endeasing="Power4.easeIn"
					style="z-index: 3"><img class="slide-img img-responsive" src="img/slideShow/s12.jpg" alt="" />
				</div>
				<!-- LAYER NR. 2.2 -->
				<div class="tp-caption customin customout"
					data-x="right"
					data-y="140"
					data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="800"
					data-start="2600"
					data-easing="Power4.easeOut"
					data-endspeed="500"
					data-endeasing="Power4.easeIn"
					style="z-index: 3"><img class="slide-img img-responsive" src="img/slideShow/s13.jpg" alt="" />
				</div>
				<!-- LAYER NR. 3 -->
				<div class="tp-caption lfl finewide_verysmall_white_mw paragraph largeblackbg br-red"
					data-x="20" 
					data-y="210"
					data-speed="1000"
					data-start="4000"
					data-easing="Power4.easeOut"
					data-endspeed="1000"
					data-endeasing="Power4.easeIn"
					style="z-index: 3; color : white !important;">Felicitări </br> Onomastice </br> Zile de naștere
				</div>
				<!-- LAYER NR. 4 -->
				<div class="tp-caption sfb"
					data-x="20" 
					data-y="335"
					data-speed="800"
					data-start="4500"
					data-easing="Power4.easeOut"
					data-endspeed="500"
					data-endeasing="Power4.easeIn"
					style="z-index: 11"><a class="btn btn-success hidden-xs" href="shopping.php">Comanda acum</a>
				</div>
			</li>
			<li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="600" >
				<!-- MAIN IMAGE -->
				<img src="img/slideShow/slide3.png" style="background-color:#fea501" alt=""  data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat">
				<!-- LAYERS NR. 1 -->
				<div class="tp-caption lfl"
					data-x="left" data-hoffset="-10"
					data-y="100"
					data-speed="800"
					data-start="1200"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><img class="img-responsive slide-img" src="img/slideShow/s35.jpg" alt="" />
				</div>
				<!-- LAYERS NR. 2 -->
				<div class="tp-caption lfl"
					data-x="left"
					data-y="160" data-hoffset="50"
					data-speed="800"
					data-start="1600"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><img class="img-responsive slide-img" src="img/slideShow/s36.jpg" alt="" />
				</div>
				<!-- LAYERS NR. 3 -->
				<div class="tp-caption lfr large_bold_grey heading white"
					data-x="right" data-hoffset="-10"
					data-y="120"
					data-speed="800"
					data-start="2000"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"
					style="text-shadow: 2px 2px #333;">Paste si Craciun
				</div>
				<!-- LAYER NR. 3 -->
				<div class="tp-caption whitedivider3px customin customout tp-resizeme"
					data-x="right" data-hoffset="-20"
					data-y="210" data-voffset="0"
					data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="700"
					data-start="2300"
					data-easing="Power3.easeInOut"
					data-splitin="none"
					data-splitout="none"
					data-elementdelay="0.1"
					data-endelementdelay="0.1"
					data-endspeed="500"
					style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
				</div>
				<!-- LAYER NR. 4 -->
				<div class="tp-caption finewide_verysmall_white_mw white customin customout tp-resizeme text-right paragraph"
					data-x="right" data-hoffset="-60"
					data-y="360" 
					data-customin="x:0;y:50;z:0;rotationX:-120;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 0%;"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="1000"
					data-start="2700"
					data-easing="Power3.easeInOut"
					data-splitin="lines"
					data-splitout="lines"
					data-elementdelay="0.2"
					data-endelementdelay="0.08"
					data-endspeed="300"
					style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">Traditiile de sarbatori pot fi un liant important in familie si o ocazie de a crea amintiri frumoase pentru mai tarziu.
				</div>
			</li>
			<li data-transition="cube" data-slotamount="7" data-masterspeed="600" >
				<!-- MAIN IMAGE -->
				<img src="img/slideShow/slide4.jpg"  alt=""  data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat">
				<!-- LAYERS NR. 1 -->
				<div class="tp-caption lfl"
					data-x="left" data-hoffset="40"
					data-y="130"
					data-speed="800"
					data-start="1500"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Power4.easeIn"
					data-captionhidden="off"><img src="img/slideShow/s31.jpg" class="img-responsive slide-img" alt="" />
				</div>
				<!-- LAYERS NR. 2 -->
				<div class="tp-caption lfl"
					data-x="left" data-hoffset="-10"
					data-y="180"
					data-speed="800"
					data-start="2200"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Power4.easeIn"
					data-captionhidden="off"><img src="img/slideShow/s33.jpg" class="img-responsive  slide-img" alt="" />
				</div>
				<!-- LAYERS NR. 3 -->
				<div class="tp-caption lfl"
					data-x="left" data-hoffset="-60"
					data-y="230"
					data-speed="800"
					data-start="2700"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Power4.easeIn"
					data-captionhidden="off"><img src="img/slideshow/s34.jpg" class="img-responsive  slide-img" alt="" />
				</div>
				<!-- LAYERS NR. 4 -->
				<div class="tp-caption sfr  thinheadline_dark white"
					data-x="right" data-hoffset="-10" 
					data-y="90"
					data-speed="800"
					data-start="3200"
					data-easing="Power4.easeOut"
					data-endspeed="500"
					data-endeasing="Power4.easeIn"
					style="z-index: 3">Cake Bar Nunta
				</div>
				<!-- LAYERS NR. 4.1 -->
				<div class="tp-caption lfr largepinkbg br-green"
					data-x="right" data-hoffset="-10"
					data-y="135"
					data-speed="800"
					data-start="3500"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off">CAKE BAR ROYAL VINTAGE
				</div>
				<!-- LAYERS NR. 5 -->
				<div class="tp-caption skewfromright medium_text text-right paragraph"
					data-x="right" data-hoffset="-10"
					data-y="225"
					data-speed="800"
					data-start="3800"
					data-easing="Power4.easeOut"
					data-endspeed="400"
					data-endeasing="Power4.easeOut"
					data-captionhidden="off">Un element central al petrecerilor,</br> locul  unde oamenii se aduna si socializeaza savurand ceva dulce,</br> CAKE BAR-ul nu trebuie sa lipseasca la niciun eveniment important din viata dumneavoastra.
				</div>
				<!-- LAYERS NR. 6 // -->
				<div class="tp-caption lfr modern_big_redbg br-red"
					data-x="right"
					data-hoffset="-10"
					data-y="315"
					data-speed="1500"
					data-start="4100"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"> CAKE BAR NOTE MUZICALE
				</div>	
				<!-- LAYERS NR. 6.1 // -->
				<div class="tp-caption lfr modern_big_redbg br-yellow"
					data-x="right"
					data-hoffset="-10"
					data-y="375"
					data-speed="1500"
					data-start="4400"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off">CAKE BAR NAVY AND BLUSH
				</div>
			</li>
		</ul>
		<!-- Banner Timer -->
		<div class="tp-bannertimer"></div>
	</div>
</div>
<!-- Slider End -->