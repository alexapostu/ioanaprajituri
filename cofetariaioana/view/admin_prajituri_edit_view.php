<!DOCTYPE html>
<html>
<head>
<!-- Title here -->
<title>Cofetaria Ioana :: Modificare prajitura</title>
<meta name="description" content="Login page Cofetaria Ioana">
<meta name="keywords" content="Your,Keywords">
		<?php include 'components/common_head.php'?>
	</head>

<body>
	<!-- Page Wrapper -->
	<div class="wrapper">
		<?php include 'components/header.php' ?>

			<!-- Main Content -->
		<div class="main-content">
			<br />
			<div class="container form-background" style="min-height: 300px">
				<div class="col-md-12">
					<form method="POST">
						<input type="hidden" value="update" name="action"/>
						<input type="hidden" value="<?php echo $model->id ?>" name="id"/>
						<div class="row row-margin-5">
							<div class="col-md-4 col-xs-4 align-right">
								Nume prajitura :
							</div>
							<div class="col-md-8 col-xs-8">
								<input class="form-control" value="<?php echo $model->nume ?>" name="nume" />
							</div>
						</div>
						<div class="row row-margin-5">
							<div class="col-md-4 col-xs-4 align-right">
								Poza:
							</div>
							<div class="col-md-8 col-xs-8">
								<input class="form-control" value="<?php echo $model->poza ?>" name="poza" />
							</div>
						</div>
						<div class="row row-margin-5">
							<div class="col-md-4 col-xs-4 align-right">
								Descriere:
							</div>
							<div class="col-md-8 col-xs-8">
								<input class="form-control" value="<?php echo $model->descriere ?>" name="descriere" />
							</div>
						</div>
						<div class="row row-margin-5">
							<div class="col-md-4 col-xs-4 align-right">
								Pret:
							</div>
							<div class="col-md-8 col-xs-8">
								<input type="number" class="form-control" value="<?php echo $model->pret ?>" name="pret" step="0.01"/>
							</div>
						</div>
						<div class="row row-margin-5">
							<div class="col-md-4 col-xs-4 align-right">
								Categorie:
							</div>
							<div class="col-md-8 col-xs-8">
								<select name="categorie" class="form-control" >
									<option value="-1">- Fara categorie -</option>
									<?php 
									for($i = 0; $i < count( $model->categorii); $i++){
										$categorie = $model->categorii[ $i ];
										$selected = '';
										if($categorie->id == $model->categorie){
											$selected = 'selected="selected"';
										}
										echo '<option ' . $selected . ' value="' . $categorie->id. '">' . $categorie->descriere . '</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="row row-margin-5">
							<div class="col-md-4 col-xs-4 align-right"></div>
							<div class="col-md-8 col-xs-8">
								<input class="btn btn-success" type="submit" value="Salveaza" />
							</div>
						</div>
					</form>				
				</div>
			</div>
			<!-- / Main Content End -->
		</div>
		</div>
		<!-- / Wrapper End -->


		<!-- Scroll to top -->
		<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span>
		<?php include 'components/scripts.php'; ?>
</body>
</html>