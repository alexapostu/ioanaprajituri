<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title>Cofetaria Ioana :: Inregistrare</title>
		<meta name="description" content="Login page Cofetaria Ioana">
		<meta name="keywords" content="Your,Keywords">
		<?php include 'components/common_head.php'?>
	</head>
	
	<body>
		<!-- Page Wrapper -->
		<div class="wrapper">
			<!-- Header Start -->
			<?php include 'components/header.php' ?>

			<!-- Main Content -->
			<div class="main-content">
				<div class="container">
					<br />
					<div class="row">
						<div class="col-md-2">
						</div>
						<div class="col-md-8 form-background">
							<?php 
							if(isset($model) && isset($model->errors) && count( $model->errors) > 0){
								for( $i = 0; $i < count( $model->errors); $i++)
								echo '<div class="form-error">' . $model->errors[$i] . '</div><br />';
							}
							?>
							
							<form role="form" action="register.php" method="POST" id="loginForm">
								<h3>Creare cont de utilizator nou</h3>
								<input type="hidden" name="action" value="inregistrare"/>
								
								<div class="form-group">
									<div class="row">
										<div class="col-md-3">Nume utilizator:</div>
										<div class="col-md-9"><input class="form-control" type="text" id="un" name="utilizator" placeholder="Nume utilizator" /></div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-3">Parola:</div>
										<div class="col-md-9"><input class="form-control" type="password" id="parola" name="parola" placeholder="Parola" /></div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-3">Repetare parola:</div>
										<div class="col-md-9"><input class="form-control" type="password" id="rep_parola" name="rep_parola" placeholder="Repetare parola" /></div>
									</div>
								</div>
								<!-- Form button -->
								<button class="btn btn-danger btn-sm" type="submit">Inregistrare</button>&nbsp;
								<button class="btn btn-default btn-sm" type="reset">Reset</button>
							</form>
						</div>
						<div class="col-md-2">
						</div>
					</div>
				</div>
			</div><!-- / Main Content End -->
			
			
		</div><!-- / Wrapper End -->
		
		
		<!-- Scroll to top -->
		<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span> 
		<?php include 'components/scripts.php'; ?>
	</body>	
</html>