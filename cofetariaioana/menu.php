<?php
require_once 'util/GenericController.php';
require_once 'services/MenuService.php';

class MenuController extends GenericController {
	public static function getDefault() {
		$model = MenuService::getMenus ();
		
		include 'view/menu_view.php';
	}
}

MenuController::resolveAction ();
?>