<?php
require_once 'util/GenericController.php';
require_once 'services/RegisterService.php';

class RegisterController extends GenericController {
	public static function getDefault() {
		include 'view/register_view.php';
	}
	
	public static function getInregistrare(){
		$model = RegisterService::inregistrare ();
		if ( $model->inregistrareReusita ) {
			include 'view/login_view.php';
		} else {
			include 'view/register_view.php';
		}
	}
}

RegisterController::resolveAction ();
?>